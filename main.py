from email import generator
from venv import create
import numpy as np
import matplotlib.pyplot as plt
import keras
from keras.models import Sequential, load_model
from keras.layers import Conv2D, Dense, MaxPooling2D, Flatten, Dropout, Reshape
from keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import confusion_matrix, classification_report
import numpy as np
import tensorflow.keras as tk
import os
from PIL import Image
from tensorflow.keras import backend as K

if not os.path.isdir("res"):
  os.makedirs("res")

def load_images(noise=0.5):
  (X_train, _), (X_test, _) = keras.datasets.mnist.load_data()
  X_train= X_train[0:1000]
  X_train = X_train.astype('float32') / 255.
  X_train_noisy = X_train + noise * np.random.normal(0., 1., size=X_train.shape)
  X_train_noisy = np.clip(X_train_noisy, 0., 1.)
  X_test= X_test[0:1000]
  X_test = X_test.astype('float32') / 255.
  X_test_noisy = X_test + noise * np.random.normal(0., 1., size=X_test.shape)
  X_test_noisy = np.clip(X_test_noisy, 0., 1.)
  return (X_train_noisy, X_train),(X_test_noisy,X_test)

def create_encoder(input_shape=(28,28)):
  model = Sequential()
  model.add(Flatten(input_shape=input_shape))
  model.add(Dense(250, activation='relu'))
  model.add(Dense(50, activation='relu'))
  print(model.summary())
  return model

def create_decoder(input_shape=(50,)):
  model = Sequential()
  model.add(Dense(50, activation='relu', input_shape=input_shape))
  model.add(Dense(250, activation='relu'))
  model.add(Dense(784, activation='sigmoid'))
  model.add(Reshape((28,28)))
  print(model.summary())
  return model

def create_auto_encoder(encoder, decoder):
  model = Sequential()
  model.add(encoder)
  model.add(decoder)
  model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
  print(model.summary())
  return model

def export_figure_matplotlib(arr, f_name, dpi=200, resize_fact=1):
    fig = plt.figure(frameon=False)
    fig.set_size_inches(arr.shape[1]/dpi, arr.shape[0]/dpi)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    plt.imshow(arr, cmap='gray_r')
    plt.savefig(f_name, dpi=(dpi * resize_fact))
    plt.close()

def printImages(examples, path):
  w = 28
  h = 28
  for i in range(len(examples)):
    tmp = examples[i].reshape(28,28)
    export_figure_matplotlib(tmp, path + "pic_" + str(i) + ".jpg", 100)

def show_plot(images, n=4, path="res/res.png"):
  for i in range(n*n):
    plt.subplot(n, n, i+1)
    tmp = images[i]
    plt.imshow(tmp, cmap='gray_r')
  plt.show()

def train_auto_encoder(auto_encoder, n_epochs=200):
  (train_noisy, train_clean),(test_noisy,test_clean) = load_images()
  auto_encoder.fit(x=train_noisy, y=train_clean, validation_data=(test_noisy, test_clean), epochs=n_epochs, batch_size=32)
  data = auto_encoder.predict(test_noisy)
  show_plot(test_noisy, 9, "res/before.png")
  show_plot(data, 9, "res/after.png")

encoder = create_encoder()
decoder = create_decoder()
auto_encoder = create_auto_encoder(encoder, decoder)
train_auto_encoder(auto_encoder, 2000)