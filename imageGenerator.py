from venv import create
import numpy as np
import matplotlib.pyplot as plt
import keras
from keras.models import Sequential, load_model
from keras.layers import Conv2D, Dense, MaxPooling2D, Flatten, Dropout
from keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import confusion_matrix, classification_report
from random import gauss
from random import seed
import numpy as np
import tensorflow.keras as tk
import numpy as np
import os

imgNb = 100
path = "images/"
pathClean = "images/clean"
pathNoisy = "images/noisy"

if not os.path.isdir(path):
  os.makedirs(path)
if not os.path.isdir(pathClean):
  os.makedirs(pathClean)
if not os.path.isdir(pathNoisy):
  os.makedirs(pathNoisy)
def loadImages():
  (trainX, trainY), (_, _) = keras.datasets.mnist.load_data()
  myfilter = np.where(trainY == 0)
  X = trainX[myfilter]
  X = X.astype('float32')
  X = X/255.0
  return X[:imgNb]

def noisy(image, index):
  row,col= image.shape
  gauss = image +  (1/(imgNb)*(index + 1)) * np.random.normal(0.,1.,(row,col))
  gauss = np.clip(gauss, 0., 1.) # < 0--> 0 ; >1 --> 1
  gauss = gauss.reshape(row,col)
  noisy = image + gauss
  return noisy

def export_figure_matplotlib(arr, f_name, dpi=200, resize_fact=1):
    fig = plt.figure(frameon=False)
    fig.set_size_inches(arr.shape[1]/dpi, arr.shape[0]/dpi)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    plt.imshow(arr, cmap='gray_r')
    plt.savefig(f_name, dpi=(dpi * resize_fact))
    plt.close()

def printImages(examples, path):
  w = 28
  h = 28
  for i in range(imgNb):
    tmp = examples[i].reshape(28,28)
    export_figure_matplotlib(tmp, path + "pic_" + str(i) + ".jpg", 100)

images = loadImages()
printImages(images, "images/clean/")
noisyImages = []
for i in range(imgNb):
  noisyImages.append(noisy(images[i], i))
printImages(noisyImages, "images/noisy/")